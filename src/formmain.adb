package body FormMain is

    --TODO:discard body if not needed
    procedure DoSomething (seconds : Duration) is 
    begin
        delay seconds;
    exception
        when others => 
            Put_Line (Standard_Error, MSG_ERROR & "FrmMain_OnDestroy");
    end;

end FormMain;
