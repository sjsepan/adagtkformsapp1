with Gtk.Window;           use Gtk.Window;
--  with Gtk.Grid;             use Gtk.Grid;
--  with Gtk.Widget;           use Gtk.Widget;
--  with Gtk.Button;           use Gtk.Button;
with Gtk.Menu_Item;        use Gtk.Menu_Item;
with Gtk.Image_Menu_Item;  use Gtk.Image_Menu_Item;
with Gtk.Tool_Button;      use Gtk.Tool_Button;
with Gtk.Label;            use Gtk.Label;
with Gtk.Progress_Bar;     use Gtk.Progress_Bar;
--  with Gtkada.Handlers;      use Gtkada.Handlers;
--  with GLib;                 use GLib;
--  with Glib.Error;           use Glib.Error;
with Ada.Text_IO;          use Ada.Text_IO;
--  with Glib.Object;

package FormMain is
   MSG_ERROR : constant String := "Ex. in ";


    -- If this form and its fields are defined in main procedure,
    --  only the field or form passed to the callback
    --  will be visible. Putting the form and its fields in a package,
    --  and referencing the package in the callback unit, 
    --  allows the callbacks to reference them.
   Form : Gtk_Window;
   MnuFileNew : Gtk_Image_Menu_Item;
   MnuFileOpen : Gtk_Image_Menu_Item;
   MnuFileSave : Gtk_Image_Menu_Item;
   MnuFileSaveAs : Gtk_Image_Menu_Item;
   MnuFilePrint : Gtk_Image_Menu_Item;
   MnuFilePrintPreview : Gtk_Image_Menu_Item;
   MnuFileQuit : Gtk_Image_Menu_Item;
   MnuEditUndo : Gtk_Image_Menu_Item;
   MnuEditRedo : Gtk_Image_Menu_Item;
   MnuEditSelectAll : Gtk_Image_Menu_Item;
   MnuEditCut : Gtk_Image_Menu_Item;
   MnuEditCopy : Gtk_Image_Menu_Item;
   MnuEditPaste : Gtk_Image_Menu_Item;
   MnuEditPasteSpecial : Gtk_Image_Menu_Item;
   MnuEditDelete : Gtk_Image_Menu_Item;
   MnuEditFind : Gtk_Image_Menu_Item;
   MnuEditReplace : Gtk_Image_Menu_Item;
   MnuEditRefresh : Gtk_Image_Menu_Item;
   MnuEditPreferences : Gtk_Image_Menu_Item;
   MnuHelpContents : Gtk_Image_Menu_Item;
   MnuHelpCheckForUpdates : Gtk_Image_Menu_Item;
   MnuHelpAbout : Gtk_Image_Menu_Item;
   TbFileNew : Gtk_Tool_Button;
   TbFileOpen : Gtk_Tool_Button;
   TbFileSave : Gtk_Tool_Button;
   TbFileSaveAs : Gtk_Tool_Button;
   TbFilePrint : Gtk_Tool_Button;
   TbEditUndo : Gtk_Tool_Button;
   TbEditRedo : Gtk_Tool_Button;
   TbEditCut : Gtk_Tool_Button;
   TbEditCopy : Gtk_Tool_Button;
   TbEditPaste : Gtk_Tool_Button;
   TbEditDelete : Gtk_Tool_Button;
   TbEditFind : Gtk_Tool_Button;
   TbEditReplace : Gtk_Tool_Button;
   TbEditRefresh : Gtk_Tool_Button;
   TbEditPreferences : Gtk_Tool_Button;
   TbHelpContents : Gtk_Tool_Button;
   LblStatusMsg : Gtk_Label;
   LblErrorMsg : Gtk_Label;
   PrgProgressBar : Gtk_Progress_Bar;

    subtype MainWindow is Gtk_Window ;

    procedure DoSomething (seconds : Duration);


end FormMain;
