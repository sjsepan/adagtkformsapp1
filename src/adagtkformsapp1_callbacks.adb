with Gtk.Window;       use Gtk.Window;
with Gtk.Widget;       use Gtk.Widget;
with Gtk.Button;           use Gtk.Button;
with Gtk.Label;            use Gtk.Label;
with Ada.Text_IO; use Ada.Text_IO;
with Gtk.Main;
with Glib.Object;
with FormMain;             use FormMain;

package body adagtkformsapp1_callbacks is

   MSG_ERROR : constant String := "Ex. in ";
   MSG_WORKING : constant String := " ...";
   MSG_CANCELLED : constant String := " cancelled.";
   MSG_DONE : constant String := " done.";
   -- If you return false in the "delete_event" signal handler,
   -- GTK will emit the "destroy" signal. Returning true means
   -- you don't want the window to be destroyed.
   --
   -- This is useful for popping up 'are you sure you want to quit?'
   -- type dialogs.
   function FrmMain_OnDeleteEvent (Self: access Gtk_Widget_Record'Class; Event: Gdk.Event.Gdk_Event) return  Boolean is
      isCancel : Boolean := False;
   begin
      Put_Line ("FrmMain_OnDeleteEvent; return False will allow Destroy");

      --This is the handler where the choice to Quit confirmation is made, 
      -- so this is where user prompt should happen, 
      -- and all handlers for quitting/closing should redirect here, 
      -- possibly by calling the form Delete to trigger this.
      isCancel := False;

      return isCancel;
   exception
      when others => 
         Put_Line (Standard_Error, MSG_ERROR & "MnuHelpAbout_OnActivate");
      return isCancel;
   end FrmMain_OnDeleteEvent;

   procedure FrmMain_OnDestroy (Self : access Gtk_Widget_Record'Class) is
   begin
      -- Gtk.Main.Main is waiting for an event to occur (like a key press or a mouse event),
      -- until Gtk.Main.Main_Quit is called.
      Put_Line ("FrmMain_OnDestroy; calls Main_Quit");
      Gtk.Main.Main_Quit;
   exception
      when others => 
         Put_Line (Standard_Error, MSG_ERROR & "MnuHelpAbout_OnActivate");
   end FrmMain_OnDestroy;

   procedure MnuFileNew_OnActivate(Self: access Gtk_Widget_Record'Class) is
   begin
      Put_Line("MnuFileNew_OnActivate");
      LblErrorMsg.Set_Label("");
      LblStatusMsg.Set_Label("New" & MSG_WORKING);
      PrgProgressBar.Set_Visible(True);
      DoSomething (1.0);
      PrgProgressBar.Set_Visible(False);
      LblStatusMsg.Set_Label (Get_Label(LblStatusMsg) & MSG_DONE);
   exception
      when others => 
         Put_Line (Standard_Error, MSG_ERROR & "MnuHelpAbout_OnActivate");
   end MnuFileNew_OnActivate;
   procedure MnuFileOpen_OnActivate(Self : access Gtk_Widget_Record'Class) is
   begin
      Put_Line("MnuFileOpen_OnActivate");
      LblErrorMsg.Set_Label("");
      LblStatusMsg.Set_Label("Open" & MSG_WORKING);
      PrgProgressBar.Set_Visible(True);
      DoSomething (1.0);
      PrgProgressBar.Set_Visible(False);
      LblStatusMsg.Set_Label (Get_Label(LblStatusMsg) & MSG_DONE);
   exception
      when others => 
         Put_Line (Standard_Error, MSG_ERROR & "MnuHelpAbout_OnActivate");
   end MnuFileOpen_OnActivate;
   procedure MnuFileSave_OnActivate(Self : access Gtk_Widget_Record'Class) is
   begin
      Put_Line("MnuFileSave_OnActivate");
      LblErrorMsg.Set_Label("");
      LblStatusMsg.Set_Label("Save" & MSG_WORKING);
      PrgProgressBar.Set_Visible(True);
      DoSomething (1.0);
      PrgProgressBar.Set_Visible(False);
      LblStatusMsg.Set_Label (Get_Label(LblStatusMsg) & MSG_DONE);
   exception
      when others => 
         Put_Line (Standard_Error, MSG_ERROR & "MnuHelpAbout_OnActivate");
   end MnuFileSave_OnActivate;
   procedure MnuFileSaveAs_OnActivate(Self : access Gtk_Widget_Record'Class) is
   begin
      Put_Line("MnuFileSaveAs_OnActivate");
      LblErrorMsg.Set_Label("");
      LblStatusMsg.Set_Label("Save As" & MSG_WORKING);
      PrgProgressBar.Set_Visible(True);
      DoSomething (1.0);
      PrgProgressBar.Set_Visible(False);
      LblStatusMsg.Set_Label (Get_Label(LblStatusMsg) & MSG_DONE);
   exception
      when others => 
         Put_Line (Standard_Error, MSG_ERROR & "MnuHelpAbout_OnActivate");
   end MnuFileSaveAs_OnActivate;
   procedure MnuFilePrint_OnActivate(Self : access Gtk_Widget_Record'Class) is
   begin
      Put_Line("MnuFilePrint_OnActivate");
      LblErrorMsg.Set_Label("");
      LblStatusMsg.Set_Label("Print" & MSG_WORKING);
      PrgProgressBar.Set_Visible(True);
      DoSomething (1.0);
      PrgProgressBar.Set_Visible(False);
      LblStatusMsg.Set_Label (Get_Label(LblStatusMsg) & MSG_DONE);
   exception
      when others => 
         Put_Line (Standard_Error, MSG_ERROR & "MnuHelpAbout_OnActivate");
   end MnuFilePrint_OnActivate;
   procedure MnuFilePrintPreview_OnActivate(Self : access Gtk_Widget_Record'Class) is
   begin
      Put_Line("MnuFilePrintPreview_OnActivate");
      LblErrorMsg.Set_Label("");
      LblStatusMsg.Set_Label("Print Preview" & MSG_WORKING);
      PrgProgressBar.Set_Visible(True);
      DoSomething (1.0);
      PrgProgressBar.Set_Visible(False);
      LblStatusMsg.Set_Label (Get_Label(LblStatusMsg) & MSG_DONE);
   exception
      when others => 
         Put_Line (Standard_Error, MSG_ERROR & "MnuHelpAbout_OnActivate");
   end MnuFilePrintPreview_OnActivate;

   procedure MnuFileQuit_OnActivate(Self: access Gtk_Menu_Item_Record'Class) is
   begin
      Put_Line("MnuFileQuit_OnActivate");
      --  LblErrorMsg.Set_Label("");
      --  LblStatusMsg.Set_Label("Quit" & MSG_WORKING);
      --  PrgProgressBar.Set_Visible(True);
      --  DoSomething (1.0);
      --  PrgProgressBar.Set_Visible(False);
      --  LblStatusMsg.Set_Label (Get_Label(LblStatusMsg) & MSG_DONE);
   exception
      when others => 
         Put_Line (Standard_Error, MSG_ERROR & "MnuHelpAbout_OnActivate");
   end MnuFileQuit_OnActivate;

   procedure MnuFileQuit_OnActivate2 (Self : access Gtk_Widget_Record'Class) is
   begin
      Put_Line ("MnuFileQuit_OnActivateItem2; calls Gtk.Window.Close on Self");
      LblErrorMsg.Set_Label("");
      LblStatusMsg.Set_Label("Quit" & MSG_WORKING);
      --TODO:this routes to FrmMain_OnDeleteEvent instead; let that prompt user.
      --  Destroy (Self); --Self is main form object, not button
      Gtk.Window.Close (Gtk_Window(Self));
   exception
      when others => 
         Put_Line (Standard_Error, MSG_ERROR & "MnuHelpAbout_OnActivate");
   end MnuFileQuit_OnActivate2;

   procedure MnuEditUndo_OnActivate(Self : access Gtk_Widget_Record'Class) is 
   begin
      Put_Line("MnuEditUndo_OnActivate");
      LblErrorMsg.Set_Label("");
      LblStatusMsg.Set_Label("Undo" & MSG_WORKING);
      PrgProgressBar.Set_Visible(True);
      DoSomething (1.0);
      PrgProgressBar.Set_Visible(False);
      LblStatusMsg.Set_Label (Get_Label(LblStatusMsg) & MSG_DONE);
   exception
      when others => 
         Put_Line (Standard_Error, MSG_ERROR & "MnuHelpAbout_OnActivate");
   end MnuEditUndo_OnActivate;
   procedure MnuEditRedo_OnActivate(Self : access Gtk_Widget_Record'Class) is 
   begin
      Put_Line("MnuEditRedo_OnActivate");
      LblErrorMsg.Set_Label("");
      LblStatusMsg.Set_Label("Redo" & MSG_WORKING);
      PrgProgressBar.Set_Visible(True);
      DoSomething (1.0);
      PrgProgressBar.Set_Visible(False);
      LblStatusMsg.Set_Label (Get_Label(LblStatusMsg) & MSG_DONE);
   exception
      when others => 
         Put_Line (Standard_Error, MSG_ERROR & "MnuHelpAbout_OnActivate");
   end MnuEditRedo_OnActivate;
   procedure MnuEditSelectAll_OnActivate(Self : access Gtk_Widget_Record'Class) is 
   begin
      Put_Line("MnuEditSelectAll_OnActivate");
      LblErrorMsg.Set_Label("");
      LblStatusMsg.Set_Label("Select All" & MSG_WORKING);
      PrgProgressBar.Set_Visible(True);
      DoSomething (1.0);
      PrgProgressBar.Set_Visible(False);
      LblStatusMsg.Set_Label (Get_Label(LblStatusMsg) & MSG_DONE);
   exception
      when others => 
         Put_Line (Standard_Error, MSG_ERROR & "MnuHelpAbout_OnActivate");
   end MnuEditSelectAll_OnActivate;
   procedure MnuEditCut_OnActivate(Self : access Gtk_Widget_Record'Class) is 
   begin
      Put_Line("MnuEditCut_OnActivate");
      LblErrorMsg.Set_Label("");
      LblStatusMsg.Set_Label("Cut" & MSG_WORKING);
      PrgProgressBar.Set_Visible(True);
      DoSomething (1.0);
      PrgProgressBar.Set_Visible(False);
      LblStatusMsg.Set_Label (Get_Label(LblStatusMsg) & MSG_DONE);
   exception
      when others => 
         Put_Line (Standard_Error, MSG_ERROR & "MnuHelpAbout_OnActivate");
   end MnuEditCut_OnActivate;
   procedure MnuEditCopy_OnActivate(Self : access Gtk_Widget_Record'Class) is 
   begin
      Put_Line("MnuEditCopy_OnActivate");
      LblErrorMsg.Set_Label("");
      LblStatusMsg.Set_Label("Copy" & MSG_WORKING);
      PrgProgressBar.Set_Visible(True);
      DoSomething (1.0);
      PrgProgressBar.Set_Visible(False);
      LblStatusMsg.Set_Label (Get_Label(LblStatusMsg) & MSG_DONE);
   exception
      when others => 
         Put_Line (Standard_Error, MSG_ERROR & "MnuHelpAbout_OnActivate");
   end MnuEditCopy_OnActivate;
   procedure MnuEditPaste_OnActivate(Self : access Gtk_Widget_Record'Class) is 
   begin
      Put_Line("MnuEditPaste_OnActivate");
      LblErrorMsg.Set_Label("");
      LblStatusMsg.Set_Label("Paste" & MSG_WORKING);
      PrgProgressBar.Set_Visible(True);
      DoSomething (1.0);
      PrgProgressBar.Set_Visible(False);
      LblStatusMsg.Set_Label (Get_Label(LblStatusMsg) & MSG_DONE);
   exception
      when others => 
         Put_Line (Standard_Error, MSG_ERROR & "MnuHelpAbout_OnActivate");
   end MnuEditPaste_OnActivate;
   procedure MnuEditPasteSpecial_OnActivate(Self : access Gtk_Widget_Record'Class) is 
   begin
      Put_Line("MnuEditPasteSpecial_OnActivate");
      LblErrorMsg.Set_Label("");
      LblStatusMsg.Set_Label("Paste Special" & MSG_WORKING);
      PrgProgressBar.Set_Visible(True);
      DoSomething (1.0);
      PrgProgressBar.Set_Visible(False);
      LblStatusMsg.Set_Label (Get_Label(LblStatusMsg) & MSG_DONE);
   exception
      when others => 
         Put_Line (Standard_Error, MSG_ERROR & "MnuHelpAbout_OnActivate");
   end MnuEditPasteSpecial_OnActivate;
   procedure MnuEditDelete_OnActivate(Self : access Gtk_Widget_Record'Class) is 
   begin
      Put_Line("MnuEditDelete_OnActivate");
      LblErrorMsg.Set_Label("");
      LblStatusMsg.Set_Label("Delete" & MSG_WORKING);
      PrgProgressBar.Set_Visible(True);
      DoSomething (1.0);
      PrgProgressBar.Set_Visible(False);
      LblStatusMsg.Set_Label (Get_Label(LblStatusMsg) & MSG_DONE);
   exception
      when others => 
         Put_Line (Standard_Error, MSG_ERROR & "MnuHelpAbout_OnActivate");
   end MnuEditDelete_OnActivate;
   procedure MnuEditFind_OnActivate(Self : access Gtk_Widget_Record'Class) is 
   begin
      Put_Line("MnuEditFind_OnActivate");
      LblErrorMsg.Set_Label("");
      LblStatusMsg.Set_Label("Find" & MSG_WORKING);
      PrgProgressBar.Set_Visible(True);
      DoSomething (1.0);
      PrgProgressBar.Set_Visible(False);
      LblStatusMsg.Set_Label (Get_Label(LblStatusMsg) & MSG_DONE);
   exception
      when others => 
         Put_Line (Standard_Error, MSG_ERROR & "MnuHelpAbout_OnActivate");
   end MnuEditFind_OnActivate;
   procedure MnuEditReplace_OnActivate(Self : access Gtk_Widget_Record'Class) is 
   begin
      Put_Line("MnuEditReplace_OnActivate");
      LblErrorMsg.Set_Label("");
      LblStatusMsg.Set_Label("Replace" & MSG_WORKING);
      PrgProgressBar.Set_Visible(True);
      DoSomething (1.0);
      PrgProgressBar.Set_Visible(False);
      LblStatusMsg.Set_Label (Get_Label(LblStatusMsg) & MSG_DONE);
   exception
      when others => 
         Put_Line (Standard_Error, MSG_ERROR & "MnuHelpAbout_OnActivate");
   end MnuEditReplace_OnActivate;
   procedure MnuEditRefresh_OnActivate(Self : access Gtk_Widget_Record'Class) is 
   begin
      Put_Line("MnuEditRefresh_OnActivate");
      LblErrorMsg.Set_Label("");
      LblStatusMsg.Set_Label("Refresh" & MSG_WORKING);
      PrgProgressBar.Set_Visible(True);
      DoSomething (1.0);
      PrgProgressBar.Set_Visible(False);
      LblStatusMsg.Set_Label (Get_Label(LblStatusMsg) & MSG_DONE);
   exception
      when others => 
         Put_Line (Standard_Error, MSG_ERROR & "MnuHelpAbout_OnActivate");
   end MnuEditRefresh_OnActivate;
   procedure MnuEditPreferences_OnActivate(Self : access Gtk_Widget_Record'Class) is 
   begin
      Put_Line("MnuEditPreferences_OnActivate");
      LblErrorMsg.Set_Label("");
      LblStatusMsg.Set_Label("Preferences" & MSG_WORKING);
      PrgProgressBar.Set_Visible(True);
      DoSomething (1.0);
      PrgProgressBar.Set_Visible(False);
      LblStatusMsg.Set_Label (Get_Label(LblStatusMsg) & MSG_DONE);
   exception
      when others => 
         Put_Line (Standard_Error, MSG_ERROR & "MnuHelpAbout_OnActivate");
   end MnuEditPreferences_OnActivate;
   procedure MnuHelpContents_OnActivate(Self : access Gtk_Widget_Record'Class) is 
   begin
      Put_Line("MnuHelpContents_OnActivate");
      LblErrorMsg.Set_Label("");
      LblStatusMsg.Set_Label("Contents" & MSG_WORKING);
      PrgProgressBar.Set_Visible(True);
      DoSomething (1.0);
      PrgProgressBar.Set_Visible(False);
      LblStatusMsg.Set_Label (Get_Label(LblStatusMsg) & MSG_DONE);
   exception
      when others => 
         Put_Line (Standard_Error, MSG_ERROR & "MnuHelpAbout_OnActivate");
   end MnuHelpContents_OnActivate;
   procedure MnuHelpCheckForUpdates_OnActivate(Self : access Gtk_Widget_Record'Class) is 
   begin
      Put_Line("MnuHelpCheckForUpdates_OnActivate");
      LblErrorMsg.Set_Label("");
      LblStatusMsg.Set_Label("Check For Updates" & MSG_WORKING);
      PrgProgressBar.Set_Visible(True);
      DoSomething (1.0);
      PrgProgressBar.Set_Visible(False);
      LblStatusMsg.Set_Label (Get_Label(LblStatusMsg) & MSG_DONE);
   exception
      when others => 
         Put_Line (Standard_Error, MSG_ERROR & "MnuHelpAbout_OnActivate");
   end MnuHelpCheckForUpdates_OnActivate;
   procedure MnuHelpAbout_OnActivate(Self : access Gtk_Widget_Record'Class) is 
   begin
      Put_Line("MnuHelpAbout_OnActivate");
      LblErrorMsg.Set_Label("");
      LblStatusMsg.Set_Label ( "About" & MSG_WORKING);
      PrgProgressBar.Set_Visible(True);
      DoSomething (1.0);
      PrgProgressBar.Set_Visible(False);
      LblStatusMsg.Set_Label (Get_Label(LblStatusMsg) & MSG_DONE);
   exception
      when others => 
         Put_Line (Standard_Error, MSG_ERROR & "MnuHelpAbout_OnActivate");
   end MnuHelpAbout_OnActivate;

end adagtkformsapp1_callbacks;
