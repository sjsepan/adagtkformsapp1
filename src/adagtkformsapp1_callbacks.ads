with Gtk.Widget;  use Gtk.Widget;
with Gtk.Window;      use Gtk.Window;
with Gtk.Button;  use Gtk.Button;
with Gtk.Label;            use Gtk.Label;
with Gtk.Menu_Item;     use Gtk.Menu_Item;
with Gtk.Image_Menu_Item; use Gtk.Image_Menu_Item;
with Glib.Object;
with Gdk.Event;


package adagtkformsapp1_callbacks is
   function FrmMain_OnDeleteEvent (Self: access Gtk_Widget_Record'Class; Event: Gdk.Event.Gdk_Event) return Boolean;
   procedure FrmMain_OnDestroy (Self: access Gtk_Widget_Record'Class);
   procedure MnuFileNew_OnActivate (Self : access Gtk_Widget_Record'Class);
   procedure MnuFileOpen_OnActivate (Self : access Gtk_Widget_Record'Class);
   procedure MnuFileSave_OnActivate (Self : access Gtk_Widget_Record'Class);
   procedure MnuFileSaveAs_OnActivate (Self : access Gtk_Widget_Record'Class);
   procedure MnuFilePrint_OnActivate (Self : access Gtk_Widget_Record'Class);
   procedure MnuFilePrintPreview_OnActivate (Self : access Gtk_Widget_Record'Class);
   --Works with Gtk_Menu_Item_Record, not Gtk_Image_Menu_Item_Record, 
   --  possibly because it could not see event on parent class(?)
   procedure MnuFileQuit_OnActivate(Self: access Gtk_Menu_Item_Record'Class);
   procedure MnuFileQuit_OnActivate2 (Self : access Gtk_Widget_Record'Class);
   procedure MnuEditUndo_OnActivate(Self : access Gtk_Widget_Record'Class);
   procedure MnuEditRedo_OnActivate(Self : access Gtk_Widget_Record'Class);
   procedure MnuEditSelectAll_OnActivate(Self : access Gtk_Widget_Record'Class);
   procedure MnuEditCut_OnActivate(Self : access Gtk_Widget_Record'Class);
   procedure MnuEditCopy_OnActivate(Self : access Gtk_Widget_Record'Class);
   procedure MnuEditPaste_OnActivate(Self : access Gtk_Widget_Record'Class);
   procedure MnuEditPasteSpecial_OnActivate(Self : access Gtk_Widget_Record'Class);
   procedure MnuEditDelete_OnActivate(Self : access Gtk_Widget_Record'Class);
   procedure MnuEditFind_OnActivate(Self : access Gtk_Widget_Record'Class);
   procedure MnuEditReplace_OnActivate(Self : access Gtk_Widget_Record'Class);
   procedure MnuEditRefresh_OnActivate(Self : access Gtk_Widget_Record'Class);
   procedure MnuEditPreferences_OnActivate(Self : access Gtk_Widget_Record'Class);
   procedure MnuHelpContents_OnActivate(Self : access Gtk_Widget_Record'Class);
   procedure MnuHelpCheckForUpdates_OnActivate(Self : access Gtk_Widget_Record'Class);
   procedure MnuHelpAbout_OnActivate(Self : access Gtk_Widget_Record'Class);
end adagtkformsapp1_callbacks;