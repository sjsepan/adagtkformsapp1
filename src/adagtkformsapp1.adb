with Gtk.Builder;          use Gtk.Builder;
with Gtk.Window;           use Gtk.Window;
--  with Gtk.Grid;             use Gtk.Grid;
with Gtk.Widget;           use Gtk.Widget;
--  with Gtk.Button;           use Gtk.Button;
--  with Gtk.Menu_Item;        use Gtk.Menu_Item;
with Gtk.Image_Menu_Item;  use Gtk.Image_Menu_Item;
with Gtk.Tool_Button;      use Gtk.Tool_Button;
with Gtk.Label;            use Gtk.Label;
with Gtk.Progress_Bar;     use Gtk.Progress_Bar;
with Gtkada.Handlers;      use Gtkada.Handlers;
with GLib;                 use GLib;
with Glib.Error;           use Glib.Error;
with Ada.Text_IO;          use Ada.Text_IO;
--  with Glib.Object;
with Gtk.Main;
with FormMain;             use FormMain;

with adagtkformsapp1_callbacks; use adagtkformsapp1_callbacks;

procedure adagtkformsapp1 is
   MSG_ERROR : constant String := "Ex. in ";


   builder : Gtk_Builder;
   returnCode : GUint;-- GLib
   fileError : aliased GError;
   
   FrmMain : MainWindow;


begin

   --  Initialize GtkAda.
   Gtk.Main.Init;

-- from gtkada-builder.ads:
--     Step 1: create a Builder and add the XML data, just as you would a
--             standard Gtk.Builder:

   -- #region FrmMain


  -- construct a Gtk_Builder instance and load our UI description
   Gtk_New (builder);
   returnCode := Add_From_File (builder, "adagtkformsapp1.glade", fileError'Access);

   if fileError /= null then
      --TODO:put to std err
      Put_Line ("Error : " & Get_Message (fileError));
      Error_Free (fileError);
      return;
   end if;

--     Step 2: add calls to "Register_Handler" to associate your handlers
--             with your callbacks.
--
  -- connect signal handlers to the constructed widgets
   FrmMain := Gtk_Window (Builder.Get_Object ("MainWindow"));--frmmain
 

--     Step 3: call Do_Connect.
--Note: not done, because we are connecting explicitly


   -- When the window emits the "delete-event" signal (which is emitted
   --  by GTK+ in response to an event coming from the window manager,
   --  usually as a result of clicking the "close" window control), we
   --  ask it to call the on_delete_event() function as defined above.
   -- This can also be triggered by a button (in this case) or menu click.
   FrmMain.On_Delete_Event (FrmMain_OnDeleteEvent'Access);

   -- connect the "destroy" signal
   -- Note: does not call Gtk.Main.Main_Quit directly
   FrmMain.On_Destroy (FrmMain_OnDestroy'Access);

   MnuFileNew := Gtk_Image_Menu_Item (Builder.Get_Object ("_menuFileNew"));
   Widget_Callback.Object_Connect (
      MnuFileNew,
      "activate",
      Widget_Callback.To_Marshaller (MnuFileNew_OnActivate'Access),
      FrmMain
   );

   MnuFileOpen := Gtk_Image_Menu_Item (Builder.Get_Object ("_menuFileOpen"));
   Widget_Callback.Object_Connect (
      MnuFileOpen,
      "activate",
      Widget_Callback.To_Marshaller (MnuFileOpen_OnActivate'Access),
      FrmMain
   );

   MnuFileSave := Gtk_Image_Menu_Item (Builder.Get_Object ("_menuFileSave"));
   Widget_Callback.Object_Connect (
      MnuFileSave,
      "activate",
      Widget_Callback.To_Marshaller (MnuFileSave_OnActivate'Access),
      FrmMain
   );

   MnuFileSaveAs := Gtk_Image_Menu_Item (Builder.Get_Object ("_menuFileSaveAs"));
   Widget_Callback.Object_Connect (
      MnuFileSaveAs,
      "activate",
      Widget_Callback.To_Marshaller (MnuFileSaveAs_OnActivate'Access),
      FrmMain
   );

   MnuFilePrint := Gtk_Image_Menu_Item (Builder.Get_Object ("_menuFilePrint"));
   Widget_Callback.Object_Connect (
      MnuFilePrint,
      "activate",
      Widget_Callback.To_Marshaller (MnuFilePrint_OnActivate'Access),
      FrmMain
   );

   MnuFilePrintPreview := Gtk_Image_Menu_Item (Builder.Get_Object ("_menuFilePrintPreview"));
   Widget_Callback.Object_Connect (
      MnuFilePrintPreview,
      "activate",
      Widget_Callback.To_Marshaller (MnuFilePrintPreview_OnActivate'Access),
      FrmMain
   );


   MnuFileQuit := Gtk_Image_Menu_Item (Builder.Get_Object ("_menuFileQuit"));
   MnuFileQuit.On_Activate (MnuFileQuit_OnActivate'Access);
   Widget_Callback.Object_Connect (
      MnuFileQuit,
      "activate",
      Widget_Callback.To_Marshaller (MnuFileQuit_OnActivate2'Access),
      FrmMain
   );

   MnuEditUndo := Gtk_Image_Menu_Item (Builder.Get_Object ("_menuEditUndo"));
   Widget_Callback.Object_Connect (
      MnuEditUndo,
      "activate",
      Widget_Callback.To_Marshaller (MnuEditUndo_OnActivate'Access),
      FrmMain
   );

   MnuEditRedo := Gtk_Image_Menu_Item (Builder.Get_Object ("_menuEditRedo"));
   Widget_Callback.Object_Connect (
      MnuEditRedo,
      "activate",
      Widget_Callback.To_Marshaller (MnuEditRedo_OnActivate'Access),
      FrmMain
   );

   MnuEditSelectAll := Gtk_Image_Menu_Item (Builder.Get_Object ("_menuEditSelectAll"));
   Widget_Callback.Object_Connect (
      MnuEditSelectAll,
      "activate",
      Widget_Callback.To_Marshaller (MnuEditSelectAll_OnActivate'Access),
      FrmMain
   );

   MnuEditCut := Gtk_Image_Menu_Item (Builder.Get_Object ("_menuEditCut"));
   Widget_Callback.Object_Connect (
      MnuEditCut,
      "activate",
      Widget_Callback.To_Marshaller (MnuEditCut_OnActivate'Access),
      FrmMain
   );

   MnuEditCopy := Gtk_Image_Menu_Item (Builder.Get_Object ("_menuEditCopy"));
   Widget_Callback.Object_Connect (
      MnuEditCopy,
      "activate",
      Widget_Callback.To_Marshaller (MnuEditCopy_OnActivate'Access),
      FrmMain
   );

   MnuEditPaste := Gtk_Image_Menu_Item (Builder.Get_Object ("_menuEditPaste"));
   Widget_Callback.Object_Connect (
      MnuEditPaste,
      "activate",
      Widget_Callback.To_Marshaller (MnuEditPaste_OnActivate'Access),
      FrmMain
   );

   MnuEditPasteSpecial := Gtk_Image_Menu_Item (Builder.Get_Object ("_menuEditPasteSpecial"));
   Widget_Callback.Object_Connect (
      MnuEditPasteSpecial,
      "activate",
      Widget_Callback.To_Marshaller (MnuEditPasteSpecial_OnActivate'Access),
      FrmMain
   );

   MnuEditDelete := Gtk_Image_Menu_Item (Builder.Get_Object ("_menuEditDelete"));
   Widget_Callback.Object_Connect (
      MnuEditDelete,
      "activate",
      Widget_Callback.To_Marshaller (MnuEditDelete_OnActivate'Access),
      FrmMain
   );

   MnuEditFind := Gtk_Image_Menu_Item (Builder.Get_Object ("_menuEditFind"));
   Widget_Callback.Object_Connect (
      MnuEditFind,
      "activate",
      Widget_Callback.To_Marshaller (MnuEditFind_OnActivate'Access),
      FrmMain
   );

   MnuEditReplace := Gtk_Image_Menu_Item (Builder.Get_Object ("_menuEditReplace"));
   Widget_Callback.Object_Connect (
      MnuEditReplace,
      "activate",
      Widget_Callback.To_Marshaller (MnuEditReplace_OnActivate'Access),
      FrmMain
   );

   MnuEditRefresh := Gtk_Image_Menu_Item (Builder.Get_Object ("_menuEditRefresh"));
   Widget_Callback.Object_Connect (
      MnuEditRefresh,
      "activate",
      Widget_Callback.To_Marshaller (MnuEditRefresh_OnActivate'Access),
      FrmMain
   );

   MnuEditPreferences := Gtk_Image_Menu_Item (Builder.Get_Object ("_menuEditPreferences"));
   Widget_Callback.Object_Connect (
      MnuEditPreferences,
      "activate",
      Widget_Callback.To_Marshaller (MnuEditPreferences_OnActivate'Access),
      FrmMain
   );

   MnuHelpContents := Gtk_Image_Menu_Item (Builder.Get_Object ("_menuHelpContents"));
   Widget_Callback.Object_Connect (
      MnuHelpContents,
      "activate",
      Widget_Callback.To_Marshaller (MnuHelpContents_OnActivate'Access),
      FrmMain
   );

   MnuHelpCheckForUpdates := Gtk_Image_Menu_Item (Builder.Get_Object ("_menuHelpCheckForUpdates"));
   Widget_Callback.Object_Connect (
      MnuHelpCheckForUpdates,
      "activate",
      Widget_Callback.To_Marshaller (MnuHelpCheckForUpdates_OnActivate'Access),
      FrmMain
   );

   MnuHelpAbout := Gtk_Image_Menu_Item (Builder.Get_Object ("_menuHelpAbout"));
   Widget_Callback.Object_Connect (
      MnuHelpAbout,
      "activate",
      Widget_Callback.To_Marshaller (MnuHelpAbout_OnActivate'Access),
      FrmMain
   );

   TbFileNew := Gtk_Tool_Button (Builder.Get_Object ("_tbFileNew"));
   Widget_Callback.Object_Connect (
      TbFileNew,
      "clicked",
      Widget_Callback.To_Marshaller (MnuFileNew_OnActivate'Access),
      FrmMain
   );

   TbFileOpen := Gtk_Tool_Button (Builder.Get_Object ("_tbFileOpen"));
   Widget_Callback.Object_Connect (
      TbFileOpen,
      "clicked",
      Widget_Callback.To_Marshaller (MnuFileOpen_OnActivate'Access),
      FrmMain
   );

   TbFileSave := Gtk_Tool_Button (Builder.Get_Object ("_tbFileSave"));
   Widget_Callback.Object_Connect (
      TbFileSave,
      "clicked",
      Widget_Callback.To_Marshaller (MnuFileSave_OnActivate'Access),
      FrmMain
   );

   TbFileSaveAs := Gtk_Tool_Button (Builder.Get_Object ("_tbFileSaveAs"));
   Widget_Callback.Object_Connect (
      TbFileSaveAs,
      "clicked",
      Widget_Callback.To_Marshaller (MnuFileSaveAs_OnActivate'Access),
      FrmMain
   );

   TbFilePrint := Gtk_Tool_Button (Builder.Get_Object ("_tbFilePrint"));
   Widget_Callback.Object_Connect (
      TbFilePrint,
      "clicked",
      Widget_Callback.To_Marshaller (MnuFilePrint_OnActivate'Access),
      FrmMain
   );

   TbEditUndo := Gtk_Tool_Button (Builder.Get_Object ("_tbEditUndo"));
   Widget_Callback.Object_Connect (
      TbEditUndo,
      "clicked",
      Widget_Callback.To_Marshaller (MnuEditUndo_OnActivate'Access),
      FrmMain
   );

   TbEditRedo := Gtk_Tool_Button (Builder.Get_Object ("_tbEditRedo"));
   Widget_Callback.Object_Connect (
      TbEditRedo,
      "clicked",
      Widget_Callback.To_Marshaller (MnuEditRedo_OnActivate'Access),
      FrmMain
   );

   TbEditCut := Gtk_Tool_Button (Builder.Get_Object ("_tbEditCut"));
   Widget_Callback.Object_Connect (
      TbEditCut,
      "clicked",
      Widget_Callback.To_Marshaller (MnuEditCut_OnActivate'Access),
      FrmMain
   );

   TbEditCopy := Gtk_Tool_Button (Builder.Get_Object ("_tbEditCopy"));
   Widget_Callback.Object_Connect (
      TbEditCopy,
      "clicked",
      Widget_Callback.To_Marshaller (MnuEditCopy_OnActivate'Access),
      FrmMain
   );

   TbEditPaste := Gtk_Tool_Button (Builder.Get_Object ("_tbEditPaste"));
   Widget_Callback.Object_Connect (
      TbEditPaste,
      "clicked",
      Widget_Callback.To_Marshaller (MnuEditPaste_OnActivate'Access),
      FrmMain
   );

   TbEditDelete := Gtk_Tool_Button (Builder.Get_Object ("_tbEditDelete"));
   Widget_Callback.Object_Connect (
      TbEditDelete,
      "clicked",
      Widget_Callback.To_Marshaller (MnuEditDelete_OnActivate'Access),
      FrmMain
   );

   TbEditFind := Gtk_Tool_Button (Builder.Get_Object ("_tbEditFind"));
   Widget_Callback.Object_Connect (
      TbEditFind,
      "clicked",
      Widget_Callback.To_Marshaller (MnuEditFind_OnActivate'Access),
      FrmMain
   );

   TbEditReplace := Gtk_Tool_Button (Builder.Get_Object ("_tbEditReplace"));
   Widget_Callback.Object_Connect (
      TbEditReplace,
      "clicked",
      Widget_Callback.To_Marshaller (MnuEditReplace_OnActivate'Access),
      FrmMain
   );

   TbEditRefresh := Gtk_Tool_Button (Builder.Get_Object ("_tbEditRefresh"));
   Widget_Callback.Object_Connect (
      TbEditRefresh,
      "clicked",
      Widget_Callback.To_Marshaller (MnuEditRefresh_OnActivate'Access),
      FrmMain
   );

   TbEditPreferences := Gtk_Tool_Button (Builder.Get_Object ("_tbEditPreferences"));
   Widget_Callback.Object_Connect (
      TbEditPreferences,
      "clicked",
      Widget_Callback.To_Marshaller (MnuEditPreferences_OnActivate'Access),
      FrmMain
   );

   TbHelpContents := Gtk_Tool_Button (Builder.Get_Object ("_tbHelpContents"));
   Widget_Callback.Object_Connect (
      TbHelpContents,
      "clicked",
      Widget_Callback.To_Marshaller (MnuHelpContents_OnActivate'Access),
      FrmMain
   );

   LblStatusMsg := Gtk_Label (builder.Get_Object ("_statusMessage"));
   LblErrorMsg := Gtk_Label (builder.Get_Object ("_errorMessage"));
   PrgProgressBar := Gtk_Progress_Bar (builder.Get_Object ("_progressBar"));

   -- Now that we are done packing our widgets, we show them all
   -- in one go, by calling FrmMain.Show_All.
   -- This call recursively calls Show on all widgets
   -- that are contained in the window, directly or indirectly.
   --  FrmMain.Show_All;
   -- Get the Gtk_Widget object from the Gtkada.Builder API
   -- for our commandWindow so it will be shown.
   Gtk.Widget.Show_All (Gtk_Widget (FrmMain));--Builder.Get_Object (Builder, "frmmain")));

   -- #endregion FrmMain

   -- All GTK applications must have a Gtk.Main.Main. This is the main message loop.
   -- Control ends here and waits for an event to occur 
   -- (like a key press or a mouse event),  until Gtk.Main.Main_Quit is called.
   Gtk.Main.Main;

--     Step 4: when the application terminates or all Windows described through
--             your builder should be closed, call Unref to free memory
--             associated with the Builder.
   Unref (builder);
   
exception
   when others => 
      Put_Line (Standard_Error, MSG_ERROR & "adagtkformsapp1");
end adagtkformsapp1;
