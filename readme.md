# Ada Gtk Glade Forms App

## About

    Demo desktop app, using Gtk via Glade with Ada, in VSCode on Linux Mint

![screenshot.png](./screenshot.png?raw=true "Screenshot")

## Setup

    Install the Ada compiler for your distro; on Linux Mint (Ubuntu, Debian), type:
`sudo apt install gnat gprbuild gdb`

    Add /usr/bin/gnat to the path by adding it at the end of your .bashrc file. Note: yours will look different, depending on what was in your path and your .bashrc. file.
`export PATH="usr/local/bin:~/bin:~/.dotnet:~/.dotnet/tools:/usr/bin/gnat:~/scripts:$PATH"`

    To ensure that it is working in the current session without re-starting the session, run that resulting line in the terminal. 


## VSCode

    Find and install the "Language Support for Ada" extension by "AdaCore":
<https://marketplace.visualstudio.com/items?itemName=AdaCore.ada>

    Make sure there is an ".objs" folder in the root of your project, or VSCode will complain that there is a problem with the toolchain. (See issues below. I have opted to use a 'obj' folder instead, and a 'bin'.)

## Gtk

`sudo apt-get install libgtkada20-dev`

`sudo apt-get install libgtkada-bin`

## Original

    The original project sample resided at

<https://zhu-qy.blogspot.com/2014/03/getting-started-with-gtkada-building.html>

    but the final version by that author resides at
<https://github.com/qunying/started-gtkada/tree/master/builder>

    Like AdaGtkHello, AdaGtkApp1 and AdaGtkGladeApp1, I've renamed files, procedures and variables to be more familiar to .Net programmers. I've also reworked the quit logic so that the buttons or menus do a Close instead of a Destroy, which allows the On Delete event handler to handle button click and window close uniformly.
    I found and used additional guidance on using the Builder at <https://brokenco.de/2017/01/07/updated-glade-gtkada-example.html>

## Form

    This form is defined in a Glade file with the extension .glade.
    If the 'code-behind' for this form and its fields are defined in main procedure, only the field or form passed to the callback will be visible. Putting the form and its fields in a package,
    and referencing the package in the callback unit, allows the callbacks to reference them. I have not determined whether the callbacks themselves should reside in the same compilation unit, and have left them separate for now.

## Compile

    If you type "gnat" at the terminal, you should get a list of the gnat commands and their equivalents. For example you can type either the platform-specific program...

`x86_64-linux-gnu-gnatmake-10 -f -u -c adagtkformsapp1`

    ...or you can use the alias...

`gnat compile adagtkformsapp1`

## Bind, Link

    I knew from experience on Windows that there would be a Link step, but did not know about Bind. The following page guided me past the initial step of compiling, to show me how to Bind/Link:

<https://gcc.gnu.org/onlinedocs/gcc-4.6.4/gnat_ugn_unw/Running-a-Simple-Ada-Program.html>

    Although the gcc command gave the error "https://gcc.gnu.org/onlinedocs/gcc-4.6.4/gnat_ugn_unw/Running-a-Simple-Ada-Program.html", it was not necessary as the Compile was already accomplished above. Do the Bind/Link with:

`gnatbind adagtkformsapp1`

`gnatlink adagtkformsapp1`

## Make

    As an alternative to the Compile, Bind, Link steps, you can Make instead, as follows:

`gnatmake  adagtkformsapp1`

## Build

    As alternative to gnatmake, you can use 

`gprbuild -P adagtkformsapp1`

## Run

    To run the executable, which is "adagtkformsapp1" without any extension, type:
`./bin/adagtkformsapp1`

## .Net

    This project does not target .Net. If you want to build for .Net (Framework), download AdaCore for .Net (Windows) (gnat-gpl-2014-dotnet-windows-bin.exe) at 
<https://community.download.adacore.com/v1/636d019a16eddc4457f1b29f4b2d2a5a21a98450?filename=gnat-2021-20210519-riscv32-elf-linux64-bin&rand=251>

## Issues

- "Unable to load project file: ~/Projects/Ada/adagtkformsapp1/adagtkformsapp1.gpr adagtkformsapp1.gpr:1:06: error: imported project file "gtkada.gpr" not found"
- "kb: warning: can't find a toolchain for the following configuration: language 'Ada', target 'x86_64-linux', default runtime"

## ToDo

- figure out GtkAda dialogs and wire up to buttons and menus
- do About box


## More Reading

<https://gcc.gnu.org/onlinedocs/gcc-4.6.0/gnat_ugn_unw/>
<https://docs.adacore.com/gprbuild-docs/html/gprbuild_ug/gnat_project_manager.html>
<https://docs.adacore.com/live/wave/gtkada/pdf/gtkada_ug/GtkAda.pdf>

## Contact

Stephen J Sepan

<sjsepan@yahoo.com>

6-27-2023
